<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'cedula', 'nombre', 'direccion', 'telefono',
    ];

    public function trips(){
        return $this->belongsToMany('App\Trip', 'client_trip', 'client_id', 'trip_id');
    }
}

