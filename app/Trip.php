<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $fillable = [
        'num_plazas', 'destino', 'lugar_origen', 'precio',
    ];

    public function clients(){
        return $this->belongsToMany('App\Client', 'client_trip', 'client_id', 'trip_id');
    }
 
}
