//Documentacion Breve URLs API Viajes

//Endpoint Cliente
+--------+-----------+----------------------+-----------------+-----------------------------------------------+------------+----------------------------------------------------------------------+
| Domain | Method    | URI                  | Name            | Action                                        | Middleware |							Description							      |	
+--------+-----------+----------------------+-----------------+-----------------------------------------------+------------+----------------------------------------------------------------------+
|        | POST      | api/clients          | clients.store   | App\Http\Controllers\ClientController@store   | api        | Listado de Clientes disponibles en BD.							      |
|        | GET|HEAD  | api/clients          | clients.index   | App\Http\Controllers\ClientController@index   | api        | Creacion de un nuevo recurso: Cliente.							      |
|        | GET|HEAD  | api/clients/{client} | clients.show    | App\Http\Controllers\ClientController@show    | api        | Mostrar un Cliente especifico, agregar el Id del cliente en la URL.  |
|        | PUT|PATCH | api/clients/{client} | clients.update  | App\Http\Controllers\ClientController@update  | api        | Actualizar un Cliente existente, agregar el Id del cliente en la URL.|
|        | DELETE    | api/clients/{client} | clients.destroy | App\Http\Controllers\ClientController@destroy | api        | Eliminar un Cliente existente, agregar el Id del cliente en la URL   |
+--------+-----------+----------------------+-----------------+-----------------------------------------------+------------+----------------------------------------------------------------------+ 

//Endpoint Viajes
+--------+-----------+----------------------+-----------------+-----------------------------------------------+------------+------------------------------------------------------------------+
| Domain | Method    | URI                  | Name            | Action                                        | Middleware | 							Description							  |	   
+--------+-----------+----------------------+-----------------+-----------------------------------------------+------------+------------------------------------------------------------------+
|        | GET|HEAD  | api/trips            | trips.index     | App\Http\Controllers\TripController@index     | api        | Listado de Viajes disponibles en BD.							  |
|        | POST      | api/trips            | trips.store     | App\Http\Controllers\TripController@store     | api        | Creacion de un nuevo recurso: Viaje.							  |
|        | GET|HEAD  | api/trips/{trip}     | trips.show      | App\Http\Controllers\TripController@show      | api        | Mostrar un Viaje especifico, agregar el Id del viaje en la URL.  |
|        | PUT|PATCH | api/trips/{trip}     | trips.update    | App\Http\Controllers\TripController@update    | api        | Actualizar un Viaje existente, agregar el Id del viaje en la URL.|
|        | DELETE    | api/trips/{trip}     | trips.destroy   | App\Http\Controllers\TripController@destroy   | api        | Eliminar un Viaje existente, agregar el Id del viaje en la URL   |
+--------+-----------+----------------------+-----------------+-----------------------------------------------+------------+------------------------------------------------------------------+