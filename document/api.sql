-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-07-2018 a las 16:59:18
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `api`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `cedula` char(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` char(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `clients`
--

INSERT INTO `clients` (`id`, `cedula`, `nombre`, `direccion`, `telefono`, `created_at`, `updated_at`) VALUES
(1, '23951835', 'Mellie Daugherty III', '99558 Schimmel Center\nNitzscheland, WA 18327-4627', '+2772264395936', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(2, '16429912', 'Richard Grady', '353 Hildegard Expressway\nJudyberg, IA 08902-4231', '+4142379236260', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(3, '20086634', 'Mr. Elmo Nader', '4040 Heller Street Apt. 006\nValerieton, FL 90491', '+2377644233748', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(4, '16556853', 'Bridget Muller', '673 Connie Mountains\nNorth Lucas, DC 41274', '+6577322295809', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(5, '14562048', 'Ed Kovacek Jr.', '289 Carmella Parkway\nNorth Maddisonchester, MI 25742-3187', '+5782303252917', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(6, '14661343', 'Gabriel Moore', '33649 Schaden Coves\nBabyfort, NE 07454-3514', '+1126991924821', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(7, '18701778', 'Wendy Klein', '219 Donald Forge\nDenisview, MT 19744-5428', '+9286539287471', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(8, '22540839', 'Allan Lowe', '867 Bennett Plaza Suite 717\nWest Cleo, ID 71008', '+7505308198088', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(9, '11863350', 'Orpha Purdy', '306 Jasmin Pike Suite 816\nCalebport, WY 69014', '+5777702138811', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(10, '16672681', 'Paula Feest III', '572 Morissette Plaza Apt. 952\nHelenafort, NC 82204', '+7332603271744', '2018-07-28 18:37:39', '2018-07-28 18:37:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client_trip`
--

CREATE TABLE `client_trip` (
  `client_id` int(10) UNSIGNED NOT NULL,
  `trip_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_07_27_175413_create_trips_table', 1),
(2, '2018_07_27_182747_create_clients_table', 1),
(3, '2018_07_27_202717_create_client_trip_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trips`
--

CREATE TABLE `trips` (
  `id` int(10) UNSIGNED NOT NULL,
  `num_plazas` int(11) NOT NULL,
  `destino` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lugar_origen` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `trips`
--

INSERT INTO `trips` (`id`, `num_plazas`, `destino`, `lugar_origen`, `precio`, `created_at`, `updated_at`) VALUES
(1, 52, 'Morocco', 'Saint Helena', '100.00', '2018-07-28 18:37:38', '2018-07-28 18:37:38'),
(2, 91, 'Azerbaijan', 'Cameroon', '100.00', '2018-07-28 18:37:38', '2018-07-28 18:37:38'),
(3, 114, 'Qatar', 'Myanmar', '100.00', '2018-07-28 18:37:38', '2018-07-28 18:37:38'),
(4, 160, 'Netherlands', 'Central African Republic', '100.00', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(5, 87, 'Tonga', 'Mali', '100.00', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(6, 123, 'Ecuador', 'Togo', '100.00', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(7, 183, 'Saint Kitts and Nevis', 'Jordan', '100.00', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(8, 106, 'Gibraltar', 'South Georgia and the South Sandwich Islands', '100.00', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(9, 104, 'Albania', 'Uruguay', '100.00', '2018-07-28 18:37:39', '2018-07-28 18:37:39'),
(10, 145, 'Angola', 'Bhutan', '100.00', '2018-07-28 18:37:39', '2018-07-28 18:37:39');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `client_trip`
--
ALTER TABLE `client_trip`
  ADD KEY `client_trip_client_id_foreign` (`client_id`),
  ADD KEY `client_trip_trip_id_foreign` (`trip_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `trips`
--
ALTER TABLE `trips`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `trips`
--
ALTER TABLE `trips`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `client_trip`
--
ALTER TABLE `client_trip`
  ADD CONSTRAINT `client_trip_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `client_trip_trip_id_foreign` FOREIGN KEY (`trip_id`) REFERENCES `trips` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
