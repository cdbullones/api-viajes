<?php

use Faker\Generator as Faker;

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'cedula' => $faker->numberBetween($min = 10000000, $max = 25000000),
        'nombre' => $faker->name,
        'direccion' => $faker->address,
        'telefono' => $faker->e164PhoneNumber,
    ];
});
