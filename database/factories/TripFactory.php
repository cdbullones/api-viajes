<?php

use Faker\Generator as Faker;

$factory->define(App\Trip::class, function (Faker $faker) {
    return [
        'num_plazas' => $faker->numberBetween($min = 50, $max = 200),
        'destino' => $faker->country,
        'lugar_origen' => $faker->country,
        'precio' => '100.00',
    ];
});
